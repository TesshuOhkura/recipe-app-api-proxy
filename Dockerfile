FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@londonappdev.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

# p : サブディレクトリも作る場合に使用するオプション
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
# nginx userはファイルを生成する権限がないので先にファイルを作成（entrypoint.sh内でdefaul.confファイルを操作する）
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
#全てのユーザーに実行権限を与える
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]
