#! /bin/sh

# スクリプト内で実行されるコマンドが正常に終了しなければ, スクリプトを即座に終了する
set -e

# 環境変数を置き換える
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# nginxをフォアグラウンドで起動させるための処理
# dockerではコマンドをforegroundで動かさないとコンテナが停止してしまうため
nginx -g 'daemon off;'